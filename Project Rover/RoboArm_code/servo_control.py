from machine import Pin,PWM
from time import sleep

activate_arm = 1

# Initialize servo pins

servo1_pin = Pin(23, Pin.OUT) # Change pin back to 23
servo2_pin = Pin(22, Pin.OUT) # Change pin back to 22
servo3_pin = Pin(19, Pin.OUT) # Change pin back to 19
servo4_pin = Pin(21, Pin.OUT) # Change pin back to 21

# Set servo PWM and freq

freq = 50
servo1 = PWM(servo1_pin, freq)
servo2 = PWM(servo2_pin, freq)
servo3 = PWM(servo3_pin, freq)
servo4 = PWM(servo4_pin, freq)

# Sero movement limits

servo1_min = 20 # 20
servo1_max = 120 # 120
servo2_min = 30 # 30
servo2_max = 70 # 70
servo3_min = 30 # 30
servo3_max = 120 # 120
servo4_min = 20 # 20
servo4_max = 45 # 45

# Setting safe position once activated

servo1.duty(20) # Activates in a "closed" position

servo2_int = int((servo2_max + servo2_min) / 2)
servo2.duty(servo2_int)

servo3_int = int((servo3_max + servo3_min) / 2)
servo3.duty(servo3_int)

servo4_int = int((servo4_max + servo4_min) / 2)
servo4.duty(servo4_int)

# Servo limits prevention

def servo1_movement(d):
    if d > servo1_min and d < servo1_max:
        servo1_set_duty = d
        servo1.duty(servo1_set_duty)
        print('Moving to ', servo1_set_duty ,)
        print('Minimum limit reached')
    elif d <= servo1_min:
        servo1_set_duty = servo1_min
        servo1.duty(servo1_set_duty)
        print('Moving to ', servo1_set_duty ,)
    elif d >= servo1_max:
        servo1_set_duty = servo1_max
        servo1.duty(servo1_set_duty)
        print('Moving to ', servo1_set_duty ,)
        print('Max limit reached')

def servo2_movement(d):
    if d > servo2_min and d < servo2_max:
        servo2_set_duty = d
        servo2.duty(servo2_set_duty)
        print('Moving to ', servo2_set_duty ,)
        print('Maximum limit reached')
    elif d <= servo2_min:
        servo2.duty(servo2_min)
        servo2_set_duty = servo2_min
        print('Moving to ', servo2_set_duty ,)
        print('Minimum limit reached')
    elif d >= servo2_max:
        servo2_set_duty = servo2_max
        servo2.duty(servo2_set_duty)
        print('Moving to ', servo2_set_duty ,)
        print('Max limit reached')
        
def servo3_movement(d):
    if d > servo3_min and d < servo3_max:
        servo3_set_duty = d
        servo3.duty(servo3_set_duty)
        print('Moving to ', servo3_set_duty ,)
        print('Maximum limit reached')
    elif d <= servo3_min:
        servo3.duty(servo3_min)
        servo3_set_duty = servo3_min
        print('Moving to ', servo3_set_duty ,)
        print('Minimum limit reached')
    elif d >= servo3_max:
        servo3_set_duty = servo3_max
        servo3.duty(servo3_set_duty)
        print('Moving to ', servo3_set_duty ,)
        print('Max limit reached')

def servo4_movement(d):
    if d > servo4_min and d < servo4_max:
        servo4_set_duty = d
        servo4.duty(servo4_set_duty)
        print('Moving to ', servo4_set_duty ,)
        print('Maximum limit reached')
    elif d <= servo4_min:
        servo4.duty(servo4_min)
        servo4_set_duty = servo4_min
        print('Moving to ', servo4_set_duty ,)
        print('Minimum limit reached')
    elif d >= servo4_max:
        servo4_set_duty = servo4_max
        servo4.duty(servo4_set_duty)
        print('Moving to ', servo4_set_duty ,)
        print('Max limit reached')

# Servo duty control

while activate_arm == 1:
    #servo1_input = int(input('Servo 1 movement: '))
    #servo2_input = int(input('Servo 2 movement: '))
    #servo3_input = int(input('Servo 3 movement: '))
    servo4_input = int(input('Servo 4 movement: '))

    #servo1_movement(servo1_input)
    #servo2_movement(servo2_input)
    #servo3_movement(servo3_input)
    servo4_movement(servo4_input)