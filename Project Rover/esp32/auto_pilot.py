from hcsr04 import HCSR04
from machine import Pin, PWM
import globalvar
import _thread
from dcmotor import DCMotor
ap_state = 0 # Sets autopilot OFF by default

sensor_left = HCSR04(trigger_pin=33,echo_pin=25,echo_timeout_us=10000)
sensor_right = HCSR04(trigger_pin=18,echo_pin=5,echo_timeout_us=10000)

frequency = 15000

#right motor
STBY_D1 = Pin(4, Pin.OUT) # Replace x

PHA_D1A = Pin(17, Pin.OUT) # Replace y
enable_D1A = PWM(Pin(16 ), frequency)

PHA_D1B = Pin(0, Pin.OUT) # Replace y
enable_D1B = PWM(Pin(15), frequency)

dc_motor_right_1 = DCMotor(STBY_D1, PHA_D1A, enable_D1A)
dc_motor_right_2 = DCMotor(STBY_D1, PHA_D1B, enable_D1B)

#left motor
STBY_D2 = Pin(14, Pin.OUT) # Replace x

PHA_D2A = Pin(13, Pin.OUT) # Replace y
enable_D2A = PWM(Pin(12), frequency)

PHA_D2B = Pin(27, Pin.OUT) # Replace y
enable_D2B = PWM(Pin(26), frequency)

dc_motor_left_1 = DCMotor(STBY_D2, PHA_D2A, enable_D2A)
dc_motor_left_2 = DCMotor(STBY_D2, PHA_D2B, enable_D2B)

def feedback(distance_left, distance_right):

    if left_dist < 10:
        print('turn right')
        turn_right()
    elif right_dist < 10:
        print('turn left')
        turn_left()
    else:
        move_forward()
        print('moving forward')


        
def turn_left2():
    dc_motor_right_1.forward(60)
    dc_motor_right_2.forward(60)
    dc_motor_left_1.backwards(0)
    dc_motor_left_2.backwards(0)

def turn_right2():
    dc_motor_right_1.backwards(0)
    dc_motor_right_2.backwards(0)
    dc_motor_left_1.forward(60)
    dc_motor_left_2.forward(60)
    
def move_forward2():
    dc_motor_right_1.forward(60)
    dc_motor_right_2.forward(60)
    dc_motor_left_1.forward(60)
    dc_motor_left_2.forward(60)

def motor_stop2():
    dc_motor_right_1.stop()
    dc_motor_right_2.stop()
    dc_motor_left_1.stop()
    dc_motor_left_2.stop()

right_dist = [0,0,0,0,0,0,0,0,0,0]
left_dist = [0,0,0,0,0,0,0,0,0,0]
true_right = 0
true_left = 0


def autopilot():

    while globalvar.autopilot_state:
        if globalvar.autopilot_state == False:
            _thread.exit()
        distance_left = sensor_left.distance_cm()
        distance_right = sensor_right.distance_cm()
        if distance_left < 1:
            print('error left')
            continue
        if distance_right < 1:
            print('error right')
            continue
        else:
            right_dist.append(distance_right)
            del right_dist[0]
            true_right = sum(right_dist) / len(right_dist) 
            print(true_right)
            left_dist.append(distance_left)
            del left_dist[0]
            true_left = sum(left_dist) / len(left_dist)
            print(true_left)
            
            if true_left < 20:
                turn_right2()
            elif true_right < 20:
                turn_left2()
            else:
                move_forward2()

            
            

        
        
'''
    else:
        feedback(distance_left, distance_right)
        print('Distance left sensor:', distance_left, 'cm' ' - ' 'Distance right sensor:', distance_right, 'cm')
        sleep(0.5)
'''
