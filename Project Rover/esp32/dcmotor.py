from machine import Pin, PWM

frequency = 15000

class DCMotor:
    def __init__(self, pin1, pin2, enable_pin, min_duty=500, max_duty=1023):
        self.pin1=pin1
        self.pin2=pin2
        self.enable_pin=enable_pin
        self.min_duty = min_duty
        self.max_duty = max_duty

    def forward(self,speed):
        self.speed = speed
        self.enable_pin.duty(self.duty_cycle(self.speed))
        self.pin1.value(1)
        self.pin2.value(1)

    def backwards(self, speed):
        self.speed = speed
        self.enable_pin.duty(self.duty_cycle(self.speed))
        self.pin1.value(1)
        self.pin2.value(0)

    def stop(self):
        self.enable_pin.duty(0)
        self.pin1.value(0)
        self.pin2.value(0)

    def duty_cycle(self, speed):
        if self.speed <= 0 or self.speed > 100:
            duty_cycle = 0
        else: duty_cycle = int(self.min_duty + (self.max_duty - self.min_duty)*((self.speed-1)/(99)))
        return duty_cycle

    def turn_left():
        dc_motor_right_1.forward(60)
        dc_motor_right_2.forward(60)
        dc_motor_left_1.backwards(60)
        dc_motor_left_2.backwards(60)

    def turn_right():
        dc_motor_right_1.backwards(60)
        dc_motor_right_2.backwards(60)
        dc_motor_left_1.forward(60)
        dc_motor_left_2.forward(60)
        
    def move_forward():
        dc_motor_right_1.forward(60)
        dc_motor_right_2.forward(60)
        dc_motor_left_1.forward(60)
        dc_motor_left_2.forward(60)

    def motor_stop():
        dc_motor_right_1.stop()
        dc_motor_right_2.stop()
        dc_motor_left_1.stop()
        dc_motor_left_2.stop()
    
# Define the left and right motor pins. Change accordingly.

#right motor
STBY_D1 = Pin(4, Pin.OUT) # Replace x

PHA_D1A = Pin(17, Pin.OUT) # Replace y
enable_D1A = PWM(Pin(16 ), frequency)

PHA_D1B = Pin(0, Pin.OUT) # Replace y
enable_D1B = PWM(Pin(15), frequency)

dc_motor_right_1 = DCMotor(STBY_D1, PHA_D1A, enable_D1A)
dc_motor_right_2 = DCMotor(STBY_D1, PHA_D1B, enable_D1B)

#left motor
STBY_D2 = Pin(14, Pin.OUT) # Replace x

PHA_D2A = Pin(13, Pin.OUT) # Replace y
enable_D2A = PWM(Pin(12), frequency)

PHA_D2B = Pin(27, Pin.OUT) # Replace y
enable_D2B = PWM(Pin(26), frequency)

dc_motor_left_1 = DCMotor(STBY_D2, PHA_D2A, enable_D2A)
dc_motor_left_2 = DCMotor(STBY_D2, PHA_D2B, enable_D2B)