from hcsr04 import HCSR04
from time import sleep

sensor_left = HCSR04(trigger_pin = 33, echo_pin = 25, echo_timeout_us = 10000)
sensor_right = HCSR04(trigger_pin = 18, echo_pin = 5, echo_timeout_us = 10000)

def feedback(distance_left, distance_right):
    if distance_left > distance_right:
        print('turn left')
    elif distance_left < distance_right:
        print('turn right')
    if distance_left == distance_right:
        print('straight')
    
        
while True:
    distance_left = sensor_left.distance_cm()
    distance_right = sensor_right.distance_cm()
    print('Distance left sensor:', distance_left, 'cm' ' - ' 'Distance right sensor:', distance_right, 'cm')
    feedback(distance_left, distance_right)
    sleep(1)
