from machine import Pin,PWM
from time import sleep

activate_arm = 1

# Initialize servo pins
servo1_pin = Pin(23, Pin.OUT)
servo2_pin = Pin(22, Pin.OUT)
servo3_pin = Pin(19, Pin.OUT)
servo4_pin = Pin(21, Pin.OUT)

# Set servo PWM and freq

freq = 50
servo1 = PWM(servo1_pin, freq)
servo2 = PWM(servo2_pin, freq)
servo3 = PWM(servo3_pin, freq)
servo4 = PWM(servo4_pin, freq)

# Sero movement limits

upper_limit = 20
lower_limit = 120
servo1_min = 60
servo1_max = 90
servo2_min = 60
servo2_max = 90
servo3_min = 60
servo3_max = 90
servo4_min = 60
servo4_max = 90

# Setting safe position once activated

servo1.duty(servo1_max + servo1_min /2)
servo2.duty(servo2_max + servo2_min /2)
servo3.duty(servo3_max + servo3_min /2)
servo4.duty(servo4_max + servo4_min /2)

# Servo limits

def servo1_movement(d):
    if d >= servo1_min and d <= servo1_max:
        servo1.duty(d)
    elif d < servo1_min:
        servo1.duty = servo1_min
    elif d > servo1_max:
        servo1.duty(servo1_max)

def servo2_movement(d):
    if d >= servo2_min and d <= servo2_max:
        servo2.duty(d)
    elif d < servo2_min:
        servo2.duty = servo2_min
    elif d > servo2_max:
        servo2.duty(servo2_max)

def servo3_movement(d):
    if d >= servo3_min and d <= servo3_max:
        servo3.duty(d)
    elif d < servo3_min:
        servo3.duty = servo3_min
    elif d > servo3_max:
        servo2.duty(servo3_max)

def servo4_movement(d):
    if d >= servo4_min and d <= servo4_max:
        servo4.duty(d)
    elif d < servo4_min:
        servo4.duty = servo4_min
    elif d > servo4_max:
        servo4.duty(servo4_max)

# Servo duty control

while activate_arm == 1:
    servo1 = input('Servo 1 movement: ')
    servo2 = input('Servo 2 movement: ')
    servo3 = input('Servo 3 movement: ')
    servo4 = input('Servo 4 movement: ')

    servo1_movement(servo1)
    servo2_movement(servo2)
    servo3_movement(servo3)
    servo4_movement(servo4)