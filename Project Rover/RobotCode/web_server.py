import time
try:
    import usocket as socket
except:
    import socket

from machine import Pin
import network
print("running script")

# Define the left and right motor pins. Change accordingly.
motorR = Pin(22,Pin.OUT)
motorL = Pin(23,Pin.OUT)

# Start with motors stopped
motorR.value(0)
motorL.value(0)

# Setup AP. Remember to change essid.
station = network.WLAN(network.AP_IF)
station.active(True)
station.config(essid = 'Team14stf')
station.config(authmode=3, password = 'fantastic')

while station.isconnected() == False:
    pass

print('Connection successful')
print(station.ifconfig())

def motorcontrol(left, right):
    motorL.value(left)
    motorR.value(right)

def web_page(request):
    motor_state = "Stopped"
    if request.find('/?forward') > 0:
        motor_state="Going Forward"
        motorcontrol(0,0)
    if request.find('/?left') > 0:
        motor_state="Going Left"
        motorcontrol(1,0)
    if request.find('/?right') > 0:
        motor_state="Going Right"
        motorcontrol(0,1)
    if request.find('/?stop') > 0:
        motor_state="Stopped"
        motorcontrol(1,1)
    html = '''<html><head> <title>Rover Web Server</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="data:,"> <style>
    html{font-family: Helvetica; display:inline-block; margin: 0px auto;
    text-align: center;}
    h1{color: #0F3376; padding: 2vh;}p{font-size: 1.5rem;}
    .button{display: inline-block; background-color: #e7bd3b; border: none;
    border-radius: 4px; color: white; text-decoration: none;
    font-size: 30px; width:100%}
    .button2{background-color: #4286f4; width:49%}
    </style></head>
    <body> <h1>Rover Web Server</h1>
    <p>Rover : <strong>""" + motor_state + """</strong></p>
    <p><a href='/?forward'><button class="button">Forward</button></a></p>
    <p><a href='/?left'><button class="button button2">LEFT</button></a>
    <a href='/?right'><button class="button button2" >RIGHT</button></a></p>
    <p><a href='/?stop'><button class="button button">STOP</button></a></p>
    </body></html>'''
    return html

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

while True:
    conn, addr = s.accept()
    print('Got a connection from %s' % str(addr))
    request = conn.recv(1024)
    request = str(request)
    print('Content = %s' % request)
    response = web_page(request)
    conn.send(response)
    conn.close()

