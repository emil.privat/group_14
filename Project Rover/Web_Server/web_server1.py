import time
try:
    import usocket as socket
except:
    import socket




from machine import Pin, PWM
import network

frequency = 15000
ap_state = 0

print("running script")


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)



# Define the left and right motor pins. Change accordingly.


#right motor
STBY_D1 = Pin(4, Pin.OUT) # Replace x

PHA_D1A = Pin(17, Pin.OUT) # Replace y
enable_D1A = PWM(Pin(16 ), frequency)

PHA_D1B = Pin(0, Pin.OUT) # Replace y
enable_D1B = PWM(Pin(15), frequency)

dc_motor_right_1 = DCMotor(STBY_D1, PHA_D1A, enable_D1A)
dc_motor_right_2 = DCMotor(STBY_D1, PHA_D1B, enable_D1B)

#left motor
STBY_D2 = Pin(14, Pin.OUT) # Replace x

PHA_D2A = Pin(13, Pin.OUT) # Replace y
enable_D2A = PWM(Pin(12), frequency)

PHA_D2B = Pin(27, Pin.OUT) # Replace y
enable_D2B = PWM(Pin(26), frequency)

dc_motor_left_1 = DCMotor(STBY_D2, PHA_D2A, enable_D2A)
dc_motor_left_2 = DCMotor(STBY_D2, PHA_D2B, enable_D2B)


# Start with motors stopped


# Setup AP. Remember to change essid.
station = network.WLAN(network.AP_IF)
station.active(True)
station.config(essid = 'Team14stf')
station.config(authmode=3, password = 'fantastic')

while station.isconnected() == False:
    pass

print('Connection successful')
print(station.ifconfig())

def motorcontrol(left, right):
    motorL.value(left)
    motorR.value(right)

def web_page(request):
    motor_state = "Stopped"
    if request.find('/?forward') > 0:
        motor_state="Going Forward"
        move_forward()

    if request.find('/?left') > 0:
        motor_state="Going Left"
        turn_left()
   

    if request.find('/?right') > 0:
        motor_state="Going Right"
        turn_right()
 

    if request.find('/?stop') > 0:
        motor_state="Stopped"
        motor_stop()

    html = '''<html><head> <title>Rover Web Server</title>
    <head>
    <title>ESP IOT DASHBOARD</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="favicon.png">
    <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body> <h1>Biohazard Rover Web Server</h1>
    
    <p>Rover : <strong>""" + motor_state + """</strong></p>
    
    <p><a href='/?forward'><button class="button">Forward</button></a></p>
    
    <p><a href='/?left'><button class="button">LEFT</button></a>
    <a href='/?right'><button class="button" >RIGHT</button></a></p>
    
    <p><a href='/?stop'><button class="button">STOP</button></a></p>
    
    <p><a href='/?activate-arm'><button class="button">Activate/Deactivate arm</button></a></p>
    <p><a href='/?Control Manual'><button class="button">Control Manual</button></a></p>
    <p><a href='/?Autopilot'><button class="button">Autopilot</button></a></p>

    <div class="topnav">
        <h1>Rover arm control</h1>
    </div>
    <div class="content">
        <div class="card-grid">
            <div class="card">
                <p class="card-title">Gripper</p>
                <p class="switch">
                    <input type="range" onchange="updateSliderPWM(this)" id="slider1" min="0" max="100" step="1" value ="0" class="slider">
                </p>
                <p class="state">Value: <span id="sliderValue1"></span></p>
            </div>
            <div class="card">
                <p class="card-title">Height</p>
                <p class="switch">
                    <input type="range" onchange="updateSliderPWM(this)" id="slider2" min="0" max="100" step="1" value ="0" class="slider">
                </p>
                <p class="state">Value: <span id="sliderValue2"></span></p>
            </div>
            <div class="card">
                <p class="card-title">Distance</p>
                <p class="switch">
                    <input type="range" onchange="updateSliderPWM(this)" id="slider3" min="0" max="100" step="1" value ="0" class="slider">
                </p>
                <p class="state">Value: <span id="sliderValue3"></span></p>
            </div>
            <div class="card">
                <p class="card-title">Pan</p>
                <p class="switch">
                    <input type="range" onchange="updateSliderPWM(this)" id="slider4" min="0" max="100" step="1" value ="0" class="slider">
                </p>
                <p class="state">Value: <span id="sliderValue4"></span></p>
            </div>
        </div>
    </div>


    </body></html>
    
    
    
    
    
    
    '''
    return html
