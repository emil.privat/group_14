# Project planning

## I. Functions

- RFID card reading
- Fingerprint scanning function
- Lock engage/disengage (for POC, we are using LEDs)
- Database (NoSQL). We will have three sets of data for fignerprint and RFID.3d Match rfid to the fingerprint we will match it
- Skeleton key RFID card, id

**If we have time/Nice to have:**
- Webserver integration for user management

## II. Connection(s) between the hardware and software

---Add Emil diagram >>> High level connection diagram---


## III. Software planning

### 1. Resources
- RFID card Writing and Reading
Check >> https://pimylifeup.com/raspberry-pi-rfid-rc522/

- RFID card reading 
Check >> https://gist.github.com/mattgorecki/6085344

- Fingerprint scanning function 
Check >> https://www.codespeedy.com/fingerprint-detection-in-python/

### 2. Start coding, use sources above

### 3. Develop databse for finger scanning 

## IV. Product testing
Check different cards and fingerprint that are not in the dataset, if it will turn on the LED or not.<br>
We will start with a low amount of RFID - fingerprint pairs.<br>


# Project Management

**Issues (4)**<br>
Non matching pairs for unlocking the system<br>
Database crushing<br>
Python errors<br>
Short circuit in the system (EXEMPLE: RFID or figerprint scanner)<br>

**Solution issue**<br>
Setting the database properly<br>
Pay attention and set it correctly at the beggining<br>
Use print function to identify where the error or return variable<br>
Wire properly, and connect the right polarity<br>

**Estimation (1-10, 10 is the highest)**<br>
2 non matching pairs for unlocking the system<br>
7 database crushing<br>
10 python errors<br>
4 short circuit in the system (EXEMPLE:RFID or figerprint)<br>


| Milestone | Deadline|
| -----------------  | ------------------ |
| 1. making RFID working | until 17 April |
| 2. making the fingerprint scanner work | until 29 April |
| 3. develop the databases | until 6th of May |
| 4. all, sync | until 17-20 of May |
| 5. Make prototype pcb (circuitboard) | until 17-20 of May |
| 6. 3D print covers | until 17-20 of May |

**MVP (Minimum viable product)**<br>
<<<<<<< HEAD
1 RFID working<br>
2 Fingerprint working<br>
3 Database working<br>
4 Prototype PCB<br>
5 Single user<br>
6 (nice to have) 3D printed case<br>
=======
1. RFID working<br>
2. fingerprint working<br>
3. Database working<br>
4. prototype PCB<br>
5. (nice to have) 3D printed case<br>
>>>>>>> 3f958d6dcc101f21f57d4d3d5c11ed99e268ecc7
