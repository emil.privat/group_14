#!/usr/bin/env python
import time
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
import mysql.connector
from PCF8574 import PCF8574_GPIO
from Adafruit_LCD1602 import Adafruit_CharLCD
import time

db = mysql.connector.connect(
  host="localhost",
  user="admin",
  passwd="secterm",
  database="login_system"
)

cursor = db.cursor()
reader = SimpleMFRC522()

def lcd_message(message):
        mcp.output(3,1)     # turn on LCD backlight
        lcd.begin(16,2)     # set number of LCD lines and columns
        lcd.clear()
        lcd.setCursor(0,0)  # set cursor position
        lcd.message(message)   # display the time
        # sleep(1)


# Initialyze LCD screen
PCF8574_address = 0x27  # I2C address of the PCF8574 chip.
PCF8574A_address = 0x3F  # I2C address of the PCF8574A chip.
# Create PCF8574 GPIO adapter.
try:
    mcp = PCF8574_GPIO(PCF8574_address)
except:
    try:
        mcp = PCF8574_GPIO(PCF8574A_address)
    except:
        print ('I2C Address Error !')
        exit(1)
# Create LCD, passing in MCP GPIO adapter.
lcd = Adafruit_CharLCD(pin_rs=0, pin_e=2, pins_db=[4,5,6,7], GPIO=mcp)

try:
  while True:
    lcd_message('Place Card to\nrecord attendance')
    print('Place Card to record attendance')
    id, text = reader.read()

    cursor.execute("Select id, name FROM users WHERE rfid_uid="+str(id))
    result = cursor.fetchone()

    if cursor.rowcount >= 1:
      lcd_message("Welcome " + result[1])
      print('Welcome ' + result[1])
      cursor.execute("INSERT INTO attendance (user_id) VALUES (%s)", (result[0],) )
      db.commit()
      time.sleep(2)
      lcd.clear()
    else:
      lcd_message("User does not exist.")
      print('User does not exist..')
      time.sleep(2)
except KeyboardInterrupt:
  print('stopping program')
finally:
  GPIO.cleanup()
