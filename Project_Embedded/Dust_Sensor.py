#DUst Sensor Sharp GP2Y1010AU0F
######################
# 1 blau LED R 150 Ohm
# 2 gr眉n LED Ground
# 3 wei脽 LED Pin 16
# 4 gelb Ground
# 5 schwarz V0  measure Pin 32
# 6 rot Vcc 3,3V
######################
from machine import Pin, ADC
from utime import sleep, sleep_us
measurePIN = ADC(Pin(32))
#measurePIN = Pin(32)
measurePIN.atten(ADC.ATTN_11DB) #range 0-4095 -> 3,3 V
LedPower = Pin(16)
samplingTime = 280 #original 280
deltaTime = 40
sleepTime = 9680
voMeasured = 0
calcVoltage = 0
dustDensity = 0
print(‘***************** START *************************’)
while True:
LedPower.off
sleep_us(samplingTime)
voMeasured = measurePIN.read() # read Dust Value
sleep_us(deltaTime)
LedPower.on
sleep_us(sleepTime)

print(‘***************** 1111111 *************************’)

#0 – 3,3 V mapped to 0 – 4095
calcVoltage = voMeasured * (3.3 / 4096)
dustDensity = 0.17 * calcVoltage – 0.01

print(“Raw Signal Value (0-4095): {0:3.2f}”.format(voMeasured))
print(” – Voltage: {0:3.2f}”.format(calcVoltage))
print(” – Dust Density: {0:3.2f}”.format(dustDensity))

sleep(1)

print(‘***************** ENDE *************************’)